

# Task1
```
The following code compiles on Linux. It has a number of problems, however. Please locate as many of those problems as you are able and provide your recommendations regarding how they can be resolved.
```

```C
typedef struct list_s
{
        struct list_s *next; /* NULL for the last item in a list */
        int data;
}
list_t;

	/* Counts the number of items in a list.
	 */
```
In **count_list_items** function recursion is not necessary. If there is a lot of entries in linked list it may cause stack overflow. This function should be rewritten as:
```C
                    //fix
                    while(head->next != NULL){count++;}
```
Also **const list_t * head** should be checked by NULL. 
```C
int count_list_items(const list_t *head) {
	if (head->next) {
		return count_list_items(head->next) + 1;
	} else {
		return 1;
	}
}

    /* Inserts a new list item after the one specified as the argument.
	 */
```
Expressions below are working the same:
```C
                    //fix
                    (item->next = malloc(sizeof(list_t)))->next = item->next;
                    //and
                    item->next = malloc(sizeof(list_t));
                    item->next->next = item->next;
```
right part will be concluded first, so insertion of element failed. Also **list_t \*item** should be checked by NULL. 
```C
void insert_next_to_list(list_t *item, int data) {
	(item->next = malloc(sizeof(list_t)))->next = item->next;
	item->next->data = data;
}
```
item->next->next will be invalidated after "free(item->next);" expression. item->next->next should be calculated before free. Also **list_t \*item** should be checked by NULL. 
```C
    /* Removes an item following the one specificed as the argument.
	 */
void remove_next_from_list(list_t *item) {
	 if (item->next) {
		free(item->next);
        item->next = item->next->next;
     }
}

	/* Returns item data as text.
	 */
```
**char buf[12];** will be insufficient on 64 bit architecture. **return buf;** returns pointer to local data, that will be invalidated after function return.
```C
char *item_data(const list_t *list)
{
	char buf[12];
	sprintf(buf, "%d", list->data);
	return buf;
}
```
# Task 2
```
By default, Linux builds user-space programs as dynamically linked applications. Please describe scenarios where you would change the default behavior to compile your application as a statically linked one.
```
You may consider building statically linked application for ease of deployment. In that way you don't need to install all necessary shared libraries on target. Also dynamically linked applications need some time at startup to load all shared libraries in a process memory. If this is a concern you may consider building statically linked application.

# Task 3
```
Develop your version of the ls utility. Only the 'ls -l' functionality is needed.
```

Solution is presented inside task_3 directory in this repository.

# Task 4
```
Please explain, at a very high-level, how a Unix OS, such as Linux, implements the break-point feature in a debugger.
```
There is a two types of breakpoints: software and hardware. For software breakpoints debugger changing code section of debugee process by modificating chosen instruction to special platform dependent instruction that throws a signal. When application is executing this instruction debugger handles interrupt signal and once again substitutes breakpoint instruction to original. To implement hardware breakpoint debugger can modificate debug registers for chosen process.

# Task 5
```
Suppose you debug a Linux application and put a break-point into a function defined by a dynamically-linked library used by the application. 
Will that break-point stop any other application that makes use of the same library?
```
No, any other applications will not stop. Before shared library is modificated all processes using one physical copy of shared library through mapping to physical memory pages to virtual. After debugger modificates shared library in a virtual memory of a process Copy On Write mechanism is triggered and debugee process will continue working with it's own "corrupt" copy.

# Task 6
```
Please translate in English. Don’t metaphrase, just put the idea as you see it into English.

Если пользовательский процесс начал операцию ввода-вывода с устройством, и остановился, ожидая окончания операции в устройстве, то операция в устройстве закончится во время работы какого-то другого процесса в системе. Аппаратное прерывание о завершении операции будет обработано операционной системой. Так как на обработку прерывания требуется некоторое время, то текущий процесс в однопроцессорной системе будет приостановлен. Таким образом, любой процесс в системе непредсказуемо влияет на производительность других процессов независимо от их приоритетов.
```

If user-space process started I/O operation with device and stopped while waiting for the this operation to be completed, then the I/O operation will be completed while some other process on the system is running. A hardware interrupt signaling the completion of the I/O operation will be handled by the operating system. Since it takes some time to process the interrupt, the current process on a single-processor system will be suspended. Thus, any process in the system unpredictably affects the performance of other processes, regardless of their priorities.

