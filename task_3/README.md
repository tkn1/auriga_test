This is a test for Auriga. Source code is located in *.h and *.c files.
# Build
```
mkdir build
make conf
make build
```

# Format with ClangFormat
```
make format
```

## Task 
Develop your version of the ls utility. Only the 'ls -l' functionality is needed.