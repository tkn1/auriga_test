#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

static int comp_dirent(const void *dirent_1, const void *dirent_2);
static void print_permissions(struct stat file_stat);
static void print_file(struct stat file_stat, char *name);
//!
//! \brief Compare function for qsort
//!
//! \param dirent_1 
//! \param dirent_2 
//!@return int 
//!
static int comp_dirent(const void *dirent_1, const void *dirent_2)
{
    if(dirent_1 == NULL)
    {
      return;
    }
    if(dirent_2 == NULL)
    {
      return;
    }
    char *name_1 = ((struct dirent *)dirent_1)->d_name;
    char *name_2 = ((struct dirent *)dirent_2)->d_name;
    return strcmp(name_1, name_2);
}
//!
//! \brief Print permission string
//!
//! \param file_stat 
//!
static void print_permissions(struct stat file_stat)
{
    printf((S_ISDIR(file_stat.st_mode)) ? "d" : "-");
    printf((file_stat.st_mode & S_IRUSR) ? "r" : "-");
    printf((file_stat.st_mode & S_IWUSR) ? "w" : "-");
    printf((file_stat.st_mode & S_IXUSR) ? "x" : "-");
    printf((file_stat.st_mode & S_IRGRP) ? "r" : "-");
    printf((file_stat.st_mode & S_IWGRP) ? "w" : "-");
    printf((file_stat.st_mode & S_IXGRP) ? "x" : "-");
    printf((file_stat.st_mode & S_IROTH) ? "r" : "-");
    printf((file_stat.st_mode & S_IWOTH) ? "w" : "-");
    printf((file_stat.st_mode & S_IXOTH) ? "x" : "-");
}
//!
//! \brief Print file info
//!
//! \param file_stat 
//! \param name 
//!
static void print_file(struct stat file_stat, char *name)
{
    print_permissions(file_stat);

    printf(" %ld", file_stat.st_nlink);

    struct passwd *pw = getpwuid(file_stat.st_uid);
    struct group *gr = getgrgid(file_stat.st_gid);
    printf(" %s", pw->pw_name);
    printf(" %s", gr->gr_name);

    printf(" %ld", file_stat.st_size);
    char *time = ctime(&file_stat.st_mtime);
    time[strcspn(time, "\r\n")] = 0;
    printf(" \t%s", time);
    name[strcspn(name, "\r\n")] = 0;
    printf(" %s", name);
    printf("\n");
}

int main()
{
    char *dir_path = ".";
    char dir_path_bar[PATH_MAX];
    strcpy(dir_path_bar, dir_path);
    strcat(dir_path_bar, "/");

    DIR *dir = opendir(dir_path);
    if (dir == NULL)
    {
        perror(dir_path);
        exit(1);
    }
    size_t total_blocks = 0;
    struct dirent *sorted_files;
    size_t file_count = 0;
    //!
    //! First count files 
    //!
    for (struct dirent *entry = readdir(dir); entry != NULL;
         entry = readdir(dir))
    {
        if (entry->d_name[0] == '.')
        {
            continue;
        }
        file_count++;
    }
    //!
    //! Rewind directory to go through files in directory again
    //!
    rewinddir(dir);
    //!
    //! Allocate array for directory entries and fill array
    //!
    sorted_files = (struct dirent *)calloc(file_count, sizeof(struct dirent));
    size_t i = 0;
    for (struct dirent *entry = readdir(dir); entry != NULL;
         entry = readdir(dir))
    {
        if (entry->d_name[0] == '.')
        {
            continue;
        }
        memcpy((void *)&sorted_files[i], (void *)entry, sizeof(struct dirent));
        i++;
    }
    //!
    //! Sort array with qsort by name
    //!
    qsort(sorted_files, file_count, sizeof(*sorted_files), comp_dirent);
    for (size_t i = 0; i < file_count; i++)
    {
        struct dirent *entry = &sorted_files[i];
        char entry_path[PATH_MAX];
        strcpy(entry_path, dir_path_bar);
        strcat(entry_path, entry->d_name);
        struct stat file_stat;
        stat(entry_path, &file_stat);
        print_file(file_stat, entry->d_name);
        size_t blocks = (file_stat.st_blocks);
        total_blocks += blocks;
    };
    printf("total %ld\n", total_blocks / 2);
    free(sorted_files);
    closedir(dir);
    return 0;
}